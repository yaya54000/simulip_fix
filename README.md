# README #

Faire la transition de simulIP Java 8 vers Java 9+.

Ce qui a été fait pour l'instant ?

-> Modification de Build.java (URLClassLoader ne fonctionne plus pareil sous Java 9+ donc j'ai cherché et modifié les lignes erronnées en "System.getProperty(src)".)

-> Correction de 3-4 exceptions qui sont apparues après (j'ai oublié lesquelles) toujours dans Build.java

-> Modification du chargement des images. On utilise maintenant ImageIO.read(new File(src))

-> Mis en commentaire la ligne qui transforme le curseur de la souris pour contourner une erreur le concernant


Le problème ?

-> Les ressources du projet chargent si Build est lancé depuis l'IDE. En revanche, au rendu du .jar, les ressources n'apparaissent plus... On obtient une fenetre sans images ni curseur.
Après avoir fais des recherches, j'ai trouvé que le soucis venait d'ImageIO, il ne faut pas lire un "new File()"
mais lire "getClass.getProperty(src)".

-> Néanmoins, cette solution (qui est normalement la bonne) ne fonctionne pas... Les images et le curseur (donc les ressources) du projet ne chargent pas.

-> L'erreur obtenue nous informe que l'input (donc l'emplacement de la photo visée) est null.


Dans ce dépot je ne met que le dossier src/ du projet simulIP

Merci !

Liens utiles : 

https://stackoverflow.com/questions/45478370/intellij-javax-imageio-iioexception-cant-read-input-file

https://github.com/Pithon3/Quest-2/issues/2

