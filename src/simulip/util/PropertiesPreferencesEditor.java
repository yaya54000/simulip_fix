package simulip.util;

import java.awt.Checkbox;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class PropertiesPreferencesEditor extends JFrame 
implements ItemListener, ActionListener {

	private static final long serialVersionUID = 448106588988456342L;

	public static final String LABEL_KEY_SUFFIX="._label";
	public static final String TIP_KEY_SUFFIX="._tip";
	
	public PropertiesPreferencesEditor(Properties properties, ResourceBundle messages){
		super();
		editedProperties=properties;
		JPanel topPanel=new JPanel();
		topPanel.setLayout(new GridBagLayout());
		add(topPanel);
		setTitle(messages.getString("simulip.util.PropertiesPreferencesEditor")); // add a conventional title suffix.... I.e: properties.getBaseName()+"_editortitle"
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel scrolledPanel=new JPanel();
		JScrollPane scrollPane = new JScrollPane(scrolledPanel);
		scrollPane.setPreferredSize(new Dimension(600, 200)); // TODO: in properties....
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		
		GridLayout spgl=new GridLayout(0,2);
		//spgl.setColumns(2);
		scrolledPanel.setLayout(spgl);
		// = (GridLayout) scrolledPanel.getLayout();

		for (String key:properties.editablePropertiesKeySet()){
			// label
			String labelStr;
			try {
				labelStr=messages.getString(key+LABEL_KEY_SUFFIX);
			}catch (MissingResourceException mre){
				labelStr=key;
			}
			String tipStr=null;
			try{
				tipStr=messages.getString(key+TIP_KEY_SUFFIX);
			}catch(Exception e){
			}
			JTextField lb=new JTextField(labelStr);
			lb.setEditable(false);
			if (tipStr!=null){
				lb.setToolTipText(tipStr);
			}
			scrolledPanel.add(lb);
			
			// edit property according to its type...
			String typeStr=properties.getPropertyType(key); // look-up in properties not in messages
			JComponent edit=null;
			if (typeStr!=null){
				if (Properties.BOOLEAN_TYPE_STR.equals(typeStr)){
					JCheckBox cb=new JCheckBox();
					cb.setSelected(properties.getOptinalBooleanProperty(key, true)); // TODO: default for typed but missing properties;
					cb.addItemListener(this);
					edit=cb;
				}
			}
			
			if (edit==null){// default: String...
				JTextField tf=new JTextField(properties.getString(key));
				tf.setEditable(true);
				tf.addActionListener(this);
				edit=tf;
			}
			edit.setName(key);
			edit.setToolTipText(key); // give info for direct edit of preference file.
			scrolledPanel.add(edit);
		}//for
		//c.fill = GridBagConstraints.HORIZONTAL;
		//add(textField, c);

		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		topPanel.add(scrollPane, gbc);
		JTextField msg=new JTextField("Use return to validate changes in text properties");
		msg.setEditable(false);
		topPanel.add(msg);
		pack();
		setAlwaysOnTop(true);
		setVisible(true);
	}
	
	private Properties editedProperties;

	@Override
	public void itemStateChanged(ItemEvent e) {
		JCheckBox cb=(JCheckBox)e.getSource();
		this.editedProperties.setPreference(cb.getName(), Boolean.toString(cb.isSelected()));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JTextField tf=(JTextField)e.getSource();
		this.editedProperties.setPreference(tf.getName(), tf.getText());
	}
}
