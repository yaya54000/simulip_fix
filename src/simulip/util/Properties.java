//    Copyright (C) 2008  Emmanuel Nataf
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    	

/* 
 * Utility abstract class  to handle resources in concrete packages
 * Not for local support... Mostly for "configuration" stuff.
 */
package simulip.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

public  class Properties {
	// additional special suffix in PROPERTIES file
	
	public static final String EDITABLE_KEY_SUFFIX="._editable"; // That also means that this property is a potential preference -- No, not in properties ;-)
	public static final String TYPE_KEY_SUFFIX="._type";
		// value for TYPE_KEY_SUFFIX properties key
		public static final String BOOLEAN_TYPE_STR="B";
		public static final String COLOR_TYPE_STR="C";
		public static final String STRING_TYPE_STR="S";
			
	/** Get the properties associated a resource name (class loader name).
	 * We use a factory method to multiple properties object for a given resource and avoid
	 * several LOCAL preferences creation of a same resource.
	 * 
	 * @param baseName a resource name in dotted notation
	 * @return
	 */
	public static Properties getProperties(String baseName){
		Properties result=builtProperties.get(baseName);
		if (result==null){
			result=new Properties(baseName);
			builtProperties.put(baseName, result); // keep the ref to the new bundle, and so ensure unicity. 
		}
		return result;
	}
	private Properties(String baseName){
		this.baseName=baseName;
		this.prefFilePath=null;
		try{
			this.configuration = ResourceBundle.getBundle(baseName);
		}catch (MissingResourceException e){
			// For Logger messages we have a chicken-eggs problem 
				LogManager.getLogManager().getLogger("").logp(Level.SEVERE, 
													 this.getClass().getName(),
													 "Properties constructor",
													 "No "+baseName+" file has not been found");
		}
		prefs=new HashMap<String, String>();
		String prefFilePath=this.getOptionalProperty("preferenceFile", "."+baseName+"."+"prefs");
		File prefFile= new File(prefFilePath);
		InputStream prefIs=null;
		if (prefFile.canRead()&&prefFile.isFile()){
			try {
				prefIs=new FileInputStream(prefFile);
			} catch (FileNotFoundException e) {
				LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
						 this.getClass().getName(),
						 "Properties constructor",
						 "preference file from properties file (or default): "+prefFilePath+"  has not been found or not readable");
			}
		}
		if (prefIs==null){
			// try base pref in user home...
			prefFilePath=System.getProperty("user.home", "~")+System.getProperty("file.separator")+prefFile.getName();
			prefFile=new File(prefFilePath);
			try {
				prefIs=new FileInputStream(prefFile);
			} catch (FileNotFoundException e) {
				LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
						 this.getClass().getName(),
						 "Properties constructor",
						 "preference file from HOME: "+prefFilePath+"  has not been found");
			}
		}
		if (prefIs==null){
			LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
					 this.getClass().getName(),
					 "Properties constructor",
					 "no preference file found....");
			this.prefFilePath=null;
		}else{
			LogManager.getLogManager().getLogger("").logp(Level.INFO, 
					 this.getClass().getName(),
					 "Properties constructor",
					 "read preferences from file:"+prefFile.getAbsolutePath());
			// suck prefBundle  into pref...
			PropertyResourceBundle prefBundle;
			this.prefFilePath=prefFilePath;
			try {
				prefBundle = new PropertyResourceBundle(prefIs);
				String k=null;
				for(Enumeration<String> ek=prefBundle.getKeys(); ek.hasMoreElements(); ){
					k=ek.nextElement();
					try{
						prefs.put(k, prefBundle.getString(k));
					}catch(Exception e){
						LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
								this.getClass().getName(),
								"Properties constructor",
							 	"internal error, can not access known key "+e);
					}
				}
			} catch (IOException ioe) {
				LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
						 this.getClass().getName(),
						 "Properties constructor",
						 "IO when reading "+prefFilePath+" preferences file. "+ioe);
				this.prefFilePath=null;
			}
		}
	}
	
	public void savePreferences() throws FileNotFoundException{
		if (this.isPreferencesChanged()){
			String prefFilePath=this.getOptionalProperty("preferenceFile", "."+baseName+"."+"prefs");
			File prefFile= new File(prefFilePath);
			PrintStream prefPs=null;
			if (prefFile.canWrite()){
				try {
					prefPs=new PrintStream(prefFile);
				} catch (Exception e) {
					LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
							 this.getClass().getName(),
							 "savePreferences",
							 "preferences file from properties file (or default): "+prefFilePath+"  is not writable: "+e);
				}
			}
			if (prefPs==null){
				// try base pref in user home... 
				//We alo have user.dir=/home/andreylocal/workspace/Test and java.class.path=/home/andreylocal/workspace/Test/bin
				prefFilePath=System.getProperty("user.home", "~")+System.getProperty("file.separator")+prefFile.getName();
				prefFile=new File(prefFilePath);
				try {
					prefPs=new PrintStream(prefFile);
				} catch (FileNotFoundException fnfe) {
					LogManager.getLogManager().getLogger("").logp(Level.SEVERE, 
							 this.getClass().getName(),
							 "Properties constructor",
							 "preference file in HOME: "+prefFilePath+"  is not writable: "+fnfe);
					// giveUp
					throw fnfe;
				}catch(SecurityException se){
					LogManager.getLogManager().getLogger("").logp(Level.SEVERE, 
							 this.getClass().getName(),
							 "Properties constructor",
							 "preference file in HOME: "+prefFilePath+"  is not writable: "+se);
					// giveUp
					throw se;
				}
			}
			for (String key:this.prefs.keySet()){
				prefPs.println(key+"="+this.prefs.get(key));
			}
		}
	}
	/** Get properties keys (without preferences ones, which should be a subset anyway)
	 * 
	 * @return
	 */
	public Set<String> propertiesKeySet(){
		return this.configuration.keySet();
	}
	
	/** Get editable (=possible preferences) properties keys
	 * 
	 * @return
	 */
	public Set<String> editablePropertiesKeySet(){
		Set<String> result=new HashSet<String>();
		Set<String> propKeys=propertiesKeySet();
		for (String key: propKeys){
			if (!key.endsWith(EDITABLE_KEY_SUFFIX) && propKeys.contains(key+EDITABLE_KEY_SUFFIX)){ // or this.getOptinalBooleanProperty(key+editableSuffix, false) ?
				result.add(key);
			}
		}
		return result;
	}
	/** Check if property is editable (= a key._editable properties exists)
	 * 
	 * Such properties are candidate to be user preferences.
	 * 
	 * @param key the property key
	 * @return true if the property with the given key is editable 
	 */
	public boolean isEditable(String key){
		// check in properties only not in preferences
		return this.configuration.containsKey(key+EDITABLE_KEY_SUFFIX);
	}
	
	/**
	 * return the type of a property (preference)
	 * @param key the property key
	 * @return the value of the key+."_type" or "S" by default
	 */
	public String getPropertyType(String key){
		// get only from properties not preferences.
		String result=STRING_TYPE_STR;
		try{
			result=this.configuration.getString(key+TYPE_KEY_SUFFIX);
		}catch(Exception e){
		}
		return result;
	}
	
	/**
	 * Set a new or change a property, which changing a PREFRENCE.
	 * @param key must not be null
	 * @param value must not be null
	 * @return true is preferences has really been updated
	 */
	public boolean setPreference(String key, String value){
		assert key!=null;
		assert value!=null;
		String oldValue=null;
		boolean result=false;
		if (isEditable(key)){
			try{
				oldValue=this.getString(key);// Get in prefs then properties
			}catch(Exception e){
			}
			if (oldValue!=null&&!oldValue.equals(value)){ 
				result=true;
				this.prefs.put(key, value);
				this.setPreferencesChanged(true);
			}
		}
		return result;
	}
	public String getProperty(String property) {
		try {
			return this.getString(property);
		} catch (Exception e){
			LogManager.getLogManager().getLogger("").logp(Level.SEVERE, 
					 this.getClass().getName(),
					 "getProperty",
					 this.baseName+ ": Mandatory String property "+property+" is missing");
			return null;
		}
	}
	
	public String getOptionalProperty(String property, String byDefault) {
		try {
			return this.getString(property);
		}catch (Exception e){
			LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
					 this.getClass().getName(),
					 "getOptionalProperty",
					 this.baseName+": Optional String property "+property+" not present");
			return byDefault;
		}
	}
	public String getOptionalProperty(String property) {
		return getOptionalProperty(property, null);
	}
	
	public int getIntProperty(String property){
		try{ 
			return Integer.parseInt(this.getString(property));
		}catch (NumberFormatException nfe){
			LogManager.getLogManager().getLogger("").logp(Level.SEVERE, 
					 this.getClass().getName(),
					 "getIntProperty",
					 this.baseName+": Mandatory integer property "+property+" as a bad format");
		}catch (Exception e){
			LogManager.getLogManager().getLogger("").logp(Level.SEVERE, 
					 this.getClass().getName(),
					 "getintProperty",
					 this.baseName+": Mandatory integer property "+property+" is missing");
		}
		return 0;
	}
	public int getOptionalIntProperty(String property, int byDefault){
		try{ 
			return Integer.parseInt(this.getString(property));
		}catch (NumberFormatException nfe){
			LogManager.getLogManager().getLogger("").logp(Level.SEVERE, 
					 this.getClass().getName(),
					 "getOptionalIntIntProperty",
					 this.baseName+": Optional integer property "+property+" as a bad format");
		}catch (Exception e){
			LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
					 this.getClass().getName(),
					 "getOptionalIntProperty",
					 this.baseName+": Option integer property "+property+" not present");
		}
		return byDefault;
	}
	public boolean getBooleanProperty(String property){
		try{ 
			return Boolean.parseBoolean(this.getString(property));
		}catch (Exception e){
			LogManager.getLogManager().getLogger("").logp(Level.SEVERE, 
					 this.getClass().getName(),
					 "getBooleanProperty",
					 this.baseName+": Mandatory boolean property "+property+" is missing");
		}
		return false;
	}
	public boolean getOptinalBooleanProperty(String property, boolean byDefault){
		try{ 
			return Boolean.parseBoolean(this.getString(property));
		}catch (Exception e){
			LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
					 this.getClass().getName(),
					 "getOptionalBooleanProperty",
					 this.baseName+": Optional boolean property "+property+" not present");
		}
		return byDefault;
	}
	
	// Read R G B Saturation color properties from a properties  file.
	
	private static String unitaryPattern="(0*(?:2[0-5]\\p{Digit})|(?:1?\\p{Digit}{1,2}))";
	private static Pattern sRGBpattern = Pattern.compile(unitaryPattern+"\\p{Blank}+"+
														 unitaryPattern+"\\p{Blank}+"+
														 unitaryPattern+"\\p{Blank}+"+
														 unitaryPattern+"(\\p{Space}*)");
	/**
	 * 
	 * @param rgbsString a R G B Sat string given as 4 decimal bytes.
	 * @return a color or null if rgbs string parameter are in in really bad format. 
	 * @throws Exception if r g b s values are in a bad interval  (not in [0-255])
	 */
	private Color getSRGBColorFromString(String rgbsString) throws NumberFormatException, IllegalArgumentException {
		Color result=null;
		Matcher m = sRGBpattern.matcher(rgbsString);
		if (m.matches()){
			// sRGBpattern has 5 groups. Group 0 is the overall match, but last group (extra space) is never return by m.group(..) ! Bug ?
			// What why we add this extra group (\\p{Space}*) to be able to get last (4th) byte group.
			result=new Color(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), 
					Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4)));
		}
		return result;
	}
	
	public Color getOptinalColorProperty(String property, Color byDefault){
		Color result=null;
		try{ 
			result=getSRGBColorFromString(this.getString(property));
		}catch (MissingResourceException mre){
			LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
					 this.getClass().getName(),
					 "getOptionalColorProperty",
					 this.baseName+": Optional Color property "+property+" not present");
		}catch(NumberFormatException nfe){
			LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
					 this.getClass().getName(),
					 "getOptionalColorProperty",
					 this.baseName+": Optional Color property "+property+" bad R G B S format (not bytes ?)");
		}catch(IllegalArgumentException iae){
			LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
					 this.getClass().getName(),
					 "getOptionalColorProperty",
					 this.baseName+": Optional Color property "+property+" bad R G B S format (not bytes ?)");
		}catch (Exception e){
			LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
					 this.getClass().getName(),
					 "getOptionalColorProperty",
					 this.baseName+": Optional Color property "+property+" other error (internal ?)");
		}
		if (result==null){
			LogManager.getLogManager().getLogger("").logp(Level.WARNING, 
					 this.getClass().getName(),
					 "getOptionalColorProperty",
					 this.baseName+": Optional Color property "+property+" really bad R G B S format");
			result=byDefault;
		}
		return byDefault;
	}
	
	public String dump(){
		String result="---- Preferences("+(this.prefFilePath==null?"**no preferences file**":this.prefFilePath)+")\n";
		for (String key:this.prefs.keySet()){
			result+=(key+"="+this.prefs.get(key)+"\n");
		}
		result+="--- Bundle("+this.baseName+")\n";
		for (Enumeration<String> keys=this.configuration.getKeys(); keys.hasMoreElements();){
			String k=(String)keys.nextElement();
			result+=(k+"="+this.configuration.getString(k)+"\n");
		}
		return result;
	}

	/** Get property as String checking first in local preferences. 
	 * 
	 */
	public String getString(String key) throws NullPointerException, 
		MissingResourceException, ClassCastException{
			String result=prefs.get(key);
			if (result==null){
				result=this.configuration.getString(key);
			}
			if (result==null){
				throw new MissingResourceException("", this.getClass().getName(), key);
			}
			return result;
	}
	public void setPreferencesChanged(boolean preferencesChanged) {
		this.preferencesChanged = preferencesChanged;
	}
	public boolean isPreferencesChanged() {
		return preferencesChanged;
	}

	private ResourceBundle configuration = null;
	private String baseName=null; // for logging
	private String prefFilePath;
	private HashMap<String, String> prefs; //HashMap: null must not be a value. TODO: use java.util.Properties ?
	private boolean preferencesChanged=false;
	
	// Map to keep instanciated Properties object...
	private static HashMap<String, Properties> builtProperties=new HashMap<String, Properties>();
}
