// <simulip : an IP and UDP simulator>
//    Copyright (C) 2008  Emmanuel Nataf
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    	
package simulip.examples;

import simulip.net.*;
import java.net.*;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UdpEchoReply extends Application{

	private byte[] data = new byte[5];
	static private ResourceBundle messages=null;
	static {
		try { 
			messages= ResourceBundle.getBundle("simulip.examples.UdpEchoMessages");
		}catch(MissingResourceException mre){
			Logger.getLogger("").logp(Level.SEVERE, 
						"simulip.examples.UdpEchoSend", "static initialization", 
						"Can not read  UdpEchoMessages (messages properties files)", mre);
		}
	}
	public void run(){
		try{
			simulip.net.DatagramSocket d = new simulip.net.DatagramSocket(this,54);
			simulip.net.DatagramPacket p = new simulip.net.DatagramPacket(data,5);
			while(true){
				d.receive(p);
				p.setAddress(p.getAddress());
				p.setPort(p.getPort());
				system.out.println(MessageFormat.format(messages.getString("recvReqFrom"), 
														new String(p.getData()), p.getAddress().getStrAddress()));
				d.send(p);
			}
		}
		catch (BindException b){
			system.out.println(b.getMessage());
		}
		
	}
}
